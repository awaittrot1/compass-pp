#!/bin/sh

set -o xtrace

if [ -z "$PREFIX" ];then
    PREFIX=/usr/local
fi

if [ "$1" = uninstall ];then
    rm "$PREFIX/share/icons/compass-pp.svg"
    rm "$PREFIX/share/applications/compass-pp.desktop"
    rm "$PREFIX/bin/compass-pp"
else
    mkdir -p "$PREFIX/share/icons"
    mkdir -p "$PREFIX/share/applications"
    mkdir -p "$PREFIX/bin"

    install -t "$PREFIX/share/icons" compass-pp.svg
    install -t "$PREFIX/share/applications" compass-pp.desktop
    install -t "$PREFIX/bin" compass-pp
fi
